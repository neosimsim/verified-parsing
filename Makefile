.PHONY:

AGDA=agda

AGDA_MAIN=src/Main.agda

AGDA_SRC=\
	src/Data/Eq.agda \
	src/Data/Nat/Aeson.agda \
	src/Data/Ord.agda \
	src/Data/SortedVec.agda \
	src/Data/SortedVec/Aeson.agda \
	src/Foreign/Haskell/Data/Aeson.agda \
	src/Foreign/Haskell/Data/ByteString/Lazy.agda \
	src/Foreign/Haskell/Data/Scientific.agda \
	src/Foreign/Haskell/Data/Text/Lazy.agda \
	src/Foreign/Haskell/Data/Text/Lazy/Encoding.agda \
	src/Foreign/Haskell/Data/Vector.agda \

all:
	$(MAKE) agda
	$(MAKE) haskell

agda: $(AGDA_SRC) $(AGDA_MAIN)

$(AGDA_SRC): phony
	$(AGDA) -c --ghc-dont-call-ghc --no-main $@

$(AGDA_MAIN): phony
	$(AGDA) -c --ghc-dont-call-ghc $@

haskell:
	cabal build

run: src/Main.agda
	cabal run

clean: phony
	rm -rf src/MAlonzo
	cabal clean

phony:
