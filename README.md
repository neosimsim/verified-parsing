## Exercises
Checkout the `exercise` branch, open [src/SortedVec.agda](src/SortedVec.agda) and
fill the goals.

## Installation
You have to install these dependencies:

* Agda
* The agda standard library

See https://agda.readthedocs.io/en/v2.6.1/getting-started/installation.html for some instructions.

## Nix setup
If you have [`nix`](https://nixos.org/download.html) installed,
you can install all necessary dependencies by simply entering a `nix-shell`.
