let
  pkgs = import ./pkgs.nix {};
in
pkgs.nixpkgs.pkgs.agdaPackages.shellFor pkgs.verified-parsing
