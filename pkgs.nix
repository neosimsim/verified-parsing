{ nixpkgs ? import (builtins.fetchTarball "https://github.com/turion/nixpkgs/archive/5396194a43028a9d76749b4491dbfcaada304d82.tar.gz") {}
}:

let
  inherit (nixpkgs) pkgs;
  inherit (pkgs) agdaPackages;
  agda = pkgs.agda.withPackages {
    pkgs = [ agdaPackages.standard-library ];
  };
  verified-parsing = agdaPackages.mkDerivation {
    version = "0.1.0.0";
    pname = "verified-parsing";
    src = ./.;
    buildPhase = "make";
    buildInputs = [ agdaPackages.standard-library ];
  };
in
{
  inherit nixpkgs agda verified-parsing;
}
