module Foreign.Haskell.Data.Aeson where

open import Data.String using (String)
open import Data.Rational using (ℚ)
open import Data.Bool using (Bool)
open import Foreign.Haskell.Data.Vector using (Vector)
open import Foreign.Haskell.Data.Scientific using (Scientific)
open import Foreign.Haskell.Data.ByteString.Lazy using (ByteString)
open import Function using (_∘_)
open import Data.List using (List)
open import Data.Maybe using (Maybe; just; nothing)
import Data.Maybe as Maybe
import Foreign.Haskell.Maybe as Maybe′
open import Data.Sum using (_⊎_; inj₁; inj₂)
import Foreign.Haskell.Either as Either
open Either using (Either)

private
  variable
    A B C : Set

{-# FOREIGN GHC
import Data.Aeson
import Data.Aeson.Types
import Data.Text as T
import Data.Either.Combinators
import Control.Monad
#-}

postulate
  HashMap : Set → Set → Set

{-# FOREIGN GHC import Data.HashMap.Strict #-}
{-# COMPILE GHC HashMap = type HashMap #-}

data Value : Set
Array = Vector Value
Object = HashMap String Value

{-# NO_POSITIVITY_CHECK #-} -- TODO necessary?
data Value where
  object : Object → Value
  array : Array → Value
  string : String → Value
  number : Scientific → Value
  bool : Bool → Value
  null : Value

{-# COMPILE GHC Value = data Data.Aeson.Value
  (Object | Array | String | Number | Bool | Null) #-}

record ToJSON (A : Set) : Set where
  field
    toJSON : A → Value

open ToJSON ⦃...⦄ public

postulate
  encodeValue : Value → ByteString

{-# COMPILE GHC encodeValue = encode #-}

encode : ⦃ ToJSON A ⦄ → A → ByteString
encode = encodeValue ∘ toJSON

infixl 25 _<*>_
infixl 25 _<$>_
infixl 30 _-:_
infixl 3 _>=>_

postulate
  Parser : Set → Set
  fail : String → Parser A
  return : A → Parser A
  _<$>_ : (A → B) → Parser A → Parser B
  mapM : (A → Parser B) → Vector A → Parser (Vector B)
  parseMaybe′ : (A → Parser B) → A → Maybe′.Maybe B
  parseEither′ : (A → Parser B) → A → Either.Either String B
  _>>=_ : Parser A → (A → Parser B) → Parser B
  _<*>_ : Parser (A → B) → Parser A → Parser B
  withObject : String → (Object → Parser A) → Value → Parser A
  withArray : String → (Array → Parser A) → Value → Parser A
  _>=>_ : (A → Parser B) → (B → Parser C) → (A → Parser C)

{-# COMPILE GHC Parser = type Parser #-}
{-# COMPILE GHC return = \ _ -> return #-}
{-# COMPILE GHC fail = \ _ msg -> fail (T.unpack msg) #-}
{-# COMPILE GHC _<$>_ = \ _ _ -> (<$>) #-}
{-# COMPILE GHC _<*>_ = \ _ _ -> (<*>) #-}
{-# COMPILE GHC mapM = \_ _ -> mapM #-}
{-# COMPILE GHC parseMaybe′ = \_ _ -> parseMaybe #-}
{-# COMPILE GHC parseEither′ = \_ _ p -> mapLeft T.pack . (parseEither p) #-}
{-# COMPILE GHC _>>=_ = \_ _ -> (>>=) #-}
{-# COMPILE GHC withObject = \_ name -> withObject (T.unpack name) #-}
{-# COMPILE GHC withArray = \_ name -> withArray (T.unpack name) #-}
{-# COMPILE GHC _>=>_ = \_ _ _ -> (>=>) #-}

parseMaybe : (A → Parser B) → A → Maybe B
parseMaybe p = Maybe′.fromForeign ∘ parseMaybe′ p

parseEither : (A → Parser B) → A → String ⊎ B
parseEither p = Either.fromForeign ∘ parseEither′ p

record FromJSON (A : Set) : Set where
  field
    parseJSON : Value → Parser A

open FromJSON ⦃...⦄ public

private
  postulate
    decodeValue′ : ByteString -> Maybe′.Maybe Value
    eitherDecodeValue′ : ByteString -> Either.Either String Value
    _-:′_ : Object → String → Parser Value

{-# COMPILE GHC decodeValue′ = decode #-}
{-# COMPILE GHC eitherDecodeValue′ = mapLeft T.pack . eitherDecode #-}
{-# COMPILE GHC _-:′_ = (.:) #-}

decodeValue : ByteString → Maybe Value
decodeValue = Maybe′.fromForeign ∘ decodeValue′

eitherDecodeValue : ByteString → String ⊎ Value
eitherDecodeValue = Either.fromForeign ∘ eitherDecodeValue′

decode : ⦃ FromJSON A ⦄ → ByteString → Maybe A
decode b = (decodeValue b) Maybe.>>= parseMaybe parseJSON

eitherDecode : ⦃ FromJSON A ⦄ → ByteString → String ⊎ A
eitherDecode b with eitherDecodeValue b
... | inj₁ err = inj₁ err
... | inj₂ v = parseEither parseJSON v

_-:_ : ⦃ FromJSON A ⦄ → Object → String → Parser A
o -: t  = (o -:′ t) >>= parseJSON
