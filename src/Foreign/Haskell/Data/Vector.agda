module Foreign.Haskell.Data.Vector where

open import Data.List using (List)

{-# FOREIGN GHC import Data.Vector as Vector #-}

private
  variable
    A B : Set

postulate
  Vector : Set → Set
  empty : Vector A
  singleton : A → Vector A
  cons : A → Vector A → Vector A
  map : (A → B) → Vector A → Vector B
  toList : Vector A → List A
  fromList : List A → Vector A
  head : Vector A → A
  tail : Vector A → Vector A

{-# FOREIGN GHC import Data.Vector #-}
{-# COMPILE GHC Vector = type Vector #-}
{-# COMPILE GHC empty = \_ -> Vector.empty #-}
{-# COMPILE GHC singleton = \_ x -> Vector.singleton x #-}
{-# COMPILE GHC cons = \_ x xs -> Vector.cons x xs #-}
{-# COMPILE GHC map = \_ _  f xs -> Vector.map f xs #-}
{-# COMPILE GHC toList = \_  xs -> Vector.toList xs #-}
{-# COMPILE GHC fromList = \_  xs -> Vector.fromList xs #-}
{-# COMPILE GHC head = \_ -> Vector.head #-}
{-# COMPILE GHC tail = \_ -> Vector.tail #-}
