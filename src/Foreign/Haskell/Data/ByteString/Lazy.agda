module Foreign.Haskell.Data.ByteString.Lazy where

postulate ByteString : Set

{-# FOREIGN GHC import Data.ByteString.Lazy #-}
{-# COMPILE GHC ByteString = type ByteString #-}
