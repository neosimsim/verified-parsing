module Foreign.Haskell.Data.Text.Lazy.Encoding where

{-# FOREIGN GHC import Data.Text.Lazy.Encoding #-}

open import Foreign.Haskell.Data.ByteString.Lazy using (ByteString)
open import Foreign.Haskell.Data.Text.Lazy using (Text)

postulate
  decodeUtf8 : ByteString → Text
  encodeUtf8 : Text → ByteString

{-# COMPILE GHC decodeUtf8 = decodeUtf8 #-}
{-# COMPILE GHC encodeUtf8 = encodeUtf8 #-}

