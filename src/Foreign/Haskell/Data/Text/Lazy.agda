module Foreign.Haskell.Data.Text.Lazy where

{-# FOREIGN GHC import Data.Text.Lazy #-}

open import Data.String using (String)

postulate
  Text : Set
  toStrict : Text → String
  fromStrict : String → Text

{-# COMPILE GHC Text = type Text #-}
{-# COMPILE GHC fromStrict = fromStrict #-}
{-# COMPILE GHC toStrict = toStrict #-}
