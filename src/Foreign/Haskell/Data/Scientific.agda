module Foreign.Haskell.Data.Scientific where

open import Data.Nat using (ℕ)

postulate
  Scientific : Set
  fromNat : ℕ → Scientific

{-# FOREIGN GHC import Data.Scientific #-}
{-# COMPILE GHC Scientific = type Scientific #-}
{-# COMPILE GHC fromNat = fromInteger #-}

