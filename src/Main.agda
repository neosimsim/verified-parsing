module Main where

import Agda.Builtin.IO as Builtin
open import Data.Unit using (⊤; tt)
open import IO

open import Data.Ord
open import Data.SortedVec
open import Data.SortedVec.Aeson
open import Data.Nat using (ℕ)
import Data.Nat as ℕ
open import Data.Nat.Aeson
open import Data.String as String using (String)
open import Function using (_$_; _∘_ )
open import Data.Sum using (_⊎_; inj₁; inj₂)

open import Foreign.Haskell.Data.ByteString.Lazy
import Foreign.Haskell.Data.Aeson as Aeson
import Foreign.Haskell.Data.Text.Lazy.Encoding as TE
import Foreign.Haskell.Data.Text.Lazy as TL

open import Relation.Binary.PropositionalEquality renaming (_≡_ to _==_)

v₁ : SortedVec ℕ 1
v₁ = 2 ∷[]

v₂ : SortedVec ℕ 2
v₂ = (3 ∷ v₁) {newHeadIsBigger = ℕ.s≤s (ℕ.s≤s ℕ.z≤n)}

v₃ : SortedVec ℕ 3
v₃ = (6 ∷ v₂) {newHeadIsBigger = ℕ.s≤s (ℕ.s≤s (ℕ.s≤s ℕ.z≤n))}

_ : String -- ByteString
_ = TL.toStrict ∘ TE.decodeUtf8 $ Aeson.encode v₃

open import Data.Maybe as Maybe hiding (_>>=_)

printVec : String ⊎ (SomeSortedVec ℕ) → IO ⊤
printVec (inj₁ err) = putStrLn err
printVec (inj₂ (mkSome sortedVec))
  = putStrLn ∘ TL.toStrict ∘ TE.decodeUtf8 $ Aeson.encode sortedVec

readVec : {A : Set} → ⦃ _ : Ord A ⦄ ⦃ _ : Aeson.FromJSON A ⦄ → String → String ⊎ (SomeSortedVec A)
readVec str = Aeson.eitherDecode ⦃ SomeSortedVecNFromJSON ⦄ (TE.encodeUtf8 (TL.fromStrict str))

v₄ : String ⊎ (SomeSortedVec ℕ)
v₄ = readVec "[6,2,1]"

open import Codata.Musical.Costring
open import Codata.Musical.Colist
open import Data.List as List
open import Data.Eq
open import Relation.Nullary using (yes; no)
open import Data.Product
open import Codata.Musical.Notation
import Level

split′ : ∀ {ℓ} → {A : Set ℓ} ⦃ _ : Eq A ⦄ → A → Colist A → ℕ → (List A) → (List A × Colist A) -- TODO return information if n=0 was reached
split′ x ys 0 acc = (acc , ys)
split′ x [] (ℕ.suc _) acc = (acc , [])
split′ x (y ∷ ys) (ℕ.suc n) acc with x ≟ y
...                    | yes _ = acc , ♭ (ys)
...                    | no _  = split′ x (♭ ys) n (acc List.++ y ∷ [])

lines : Costring → Colist String
lines [] = []
lines text with split′ '\n' text 1024 []
... | line , rest = String.fromList line ∷ (♯ (lines rest))

main : Builtin.IO ⊤
main = run $ ♯ getContents >>= λ x → ♯ ((♯ (mapM′ (printVec ∘ readVec) (lines x))) >>= (λ y → ♯ return tt ))
  -- content <- ♯ getContents
  -- foo <- (mapM′ ? ? )
 -- ?
-- ♯ {!mapM′ putStrLn!}
 -- run ∘ putStrLn ∘ TL.toStrict ∘ TE.decodeUtf8 $ Aeson.encode v₃
 -- run $ printVec v₄

