module Data.Eq where

import Level
open Level using (Level)
open import Relation.Nullary using (Dec)
open import Relation.Binary

record Eq {ℓ : Level} (A : Set ℓ) : Set (Level.suc ℓ) where
  field
    _≡_ : Rel A ℓ
    ≡-isDecEquivalence : IsDecEquivalence _≡_

  _≟_ = IsDecEquivalence._≟_ ≡-isDecEquivalence

open Eq ⦃...⦄ public

open import Data.Nat using (ℕ)
import Data.Nat.Properties as ℕ
import Relation.Binary.PropositionalEquality as PE renaming (_≡_ to _==_)
open PE using (_==_)

instance
  NatEq : Eq ℕ
  _≡_ ⦃ NatEq ⦄ = _==_
  ≡-isDecEquivalence ⦃ NatEq ⦄ = ℕ.≡-isDecEquivalence

open import Data.Char as Char hiding (_==_)

instance
  CharEq : Eq Char
  _≡_ ⦃ CharEq ⦄ = _==_
  ≡-isDecEquivalence ⦃ CharEq ⦄ = PE.isDecEquivalence Char._≟_
