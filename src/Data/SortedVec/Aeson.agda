module Data.SortedVec.Aeson where

open import Data.SortedVec as SortedVec
import Data.Vec as Vec
open import Data.Nat using (ℕ)
import Data.Nat as ℕ
open import Data.Nat.Aeson
open import Data.Ord
open import Data.Maybe using (just; nothing)
import Data.List as List
open import Data.List.Aeson
open import Function using (_∘_; _$_)
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

import Foreign.Haskell.Data.Vector as Vector
import Foreign.Haskell.Data.Aeson as Aeson
open Aeson using (_<$>_; _<*>_; _>>=_; return; _-:_; mapM; withArray; withObject; ToJSON; toJSON; FromJSON; parseJSON)

private
  variable
    A : Set
    n : ℕ

private
  toVector : ⦃ _ : Ord A ⦄ → SortedVec A n → Vector.Vector A
  toVector [] = Vector.empty
  toVector (x ∷[]) = Vector.singleton x
  toVector (x ∷ xs) = Vector.cons x (toVector xs)

fromListWithProof : (xs : List.List A) → (List.length xs ≡ n) → Vec.Vec A n
fromListWithProof xs refl = Vec.fromList xs

instance

  SortedVecToJSON : ⦃ _ : Ord A ⦄ ⦃ _ : ToJSON A ⦄ → ToJSON (SortedVec A n)
  toJSON ⦃ SortedVecToJSON ⦄ = Aeson.array ∘ Vector.map toJSON ∘ toVector

  VecFromJSON : ⦃ _ : FromJSON A ⦄ → {n : ℕ} → FromJSON (Vec.Vec A n)
  parseJSON ⦃ VecFromJSON {n = n} ⦄ = withArray "Vec" $ λ x → do
    list ← parseJSON (Aeson.array x)
    yes p ← return $ List.length list ℕ.≟ n
      where no _ → Aeson.fail "length mismatch"
    return (fromListWithProof list p)

  SortedVecFromJSON : ⦃ _ : Ord A ⦄ ⦃ _ : FromJSON A ⦄ {n : ℕ} → FromJSON (SortedVec A n)
  parseJSON ⦃ SortedVecFromJSON ⦄ = withArray "SortedVec" $ λ x → do
    just vec ← isSorted? <$> parseJSON (Aeson.array x)
      where nothing → Aeson.fail "parsing SortedVec failed, data are not sorted"
    return vec

  SomeSortedVecFromJSON : ⦃ _ : Ord A ⦄ → ⦃ FromJSON A ⦄ → FromJSON (SomeSortedVec A)
  parseJSON ⦃ SomeSortedVecFromJSON ⦄ = withArray "SomeSortedVec" $ λ x → do
   just vec ← fromList <$> parseJSON (Aeson.array x)
     where nothing → Aeson.fail "parsing SomeSortedVec failed, data are not sorted"
   return vec

  SomeSortedVecNFromJSON : ⦃ _ : Ord A ⦄ → ⦃ FromJSON A ⦄ → FromJSON (SomeSortedVec A)
  parseJSON ⦃ SomeSortedVecNFromJSON ⦄ = withObject "SomeSortedVecN" $ λ x → do
    length <- x -: "length"
    values <- x -: "values"
    return (mkSome′ length values)
