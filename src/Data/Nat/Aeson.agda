module Data.Nat.Aeson where

open import Data.Nat
import Foreign.Haskell.Data.Aeson as Aeson
import Foreign.Haskell.Data.Scientific as Scientific
open import Function using (_∘_)

{-# FOREIGN GHC
import Data.Aeson
#-}

instance
  NatToJSON : Aeson.ToJSON ℕ
  NatToJSON = record { toJSON = Aeson.number ∘ Scientific.fromNat }

private
  postulate
    parseJSON : Aeson.Value → Aeson.Parser ℕ

{-# COMPILE GHC parseJSON = parseJSON #-}

instance
  NatFromJSON : Aeson.FromJSON ℕ
  NatFromJSON = record { parseJSON = parseJSON }
