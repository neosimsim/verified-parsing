module Data.SortedVec where

open import Data.Ord using (Ord; _≤_; _≤?_; ≤-isDecTotalOrder; ≤-trans; _≤!_; ≤-refl)
open import Data.Eq using (_≟_)
import Data.Nat as ℕ
open ℕ using (ℕ)
import Data.Nat.Properties as ℕ
open import Relation.Nullary
open import Relation.Nullary.Sum using (_¬-⊎_)
open import Data.Sum
open import Relation.Binary.PropositionalEquality hiding (trans)
open import Data.Unit using (⊤; tt)
open import Data.Product using (_×_; _,_)
open import Data.Fin using (Fin)
open import Relation.Binary using (Decidable)
import Data.Fin as Fin
open Data.Fin using (Fin′)
open import Data.Maybe using (Maybe; just; nothing)
open import Data.Vec using (Vec; _∷_; [])
import Data.Vec as Vec
open import Data.Product using (proj₁; proj₂)
open import Function using (_∘_)

private
  variable
    A : Set
    x y : A
    n m : ℕ

data SortedVec (A : Set) ⦃ _ : Ord A ⦄ : ℕ → Set
head : ⦃ _ : Ord A ⦄ → SortedVec A (ℕ.suc n) → A

infixr 5 _∷_
data SortedVec A where
  []  : SortedVec A 0
  _∷[] : A → SortedVec A 1
  _∷_ : (x : A)
      → (xs : SortedVec A (ℕ.suc n))
      → {newHeadIsBigger : head xs ≤ x}
      → SortedVec A (ℕ.suc (ℕ.suc n))

head (x ∷[]) = x
head (x ∷ _) = x

infixr 26 _!!_
_!!_ : ⦃ _ : Ord A ⦄ → SortedVec A n → (m : Fin n) → A
(x ∷[]) !! Fin.zero = x
(x ∷ xs) !! Fin.zero = x
(x ∷ xs) !! Fin.suc i = xs !! i

vᵢ≤head : ⦃ _ : Ord A ⦄
     → (v : SortedVec A (ℕ.suc n))
     → (i : Fin (ℕ.suc n))
     → v !! i ≤ head v
vᵢ≤head (x ∷[]) Fin.zero = ≤-refl
vᵢ≤head (x ∷ v) Fin.zero = ≤-refl
vᵢ≤head ((x ∷ v) {newHeadIsBigger = head:v≤x}) (Fin.suc i) = ≤-trans (vᵢ≤head v i) head:v≤x

sorted : ⦃ _ : Ord A ⦄
       → (v : SortedVec A n)
       → (i : Fin n)  -- i < n
       → (j : Fin′ i) -- j < i
       → (v !! i) ≤ (v !! Fin.inject j)
sorted xs@(_ ∷ _) i@(Fin.suc _) Fin.zero = vᵢ≤head xs i
sorted (_ ∷ xs) (Fin.suc i) (Fin.suc j) = sorted xs i j

private
  -- | @InsertResult x v@ stored information when @x@ is @insert@ed into @v@.
  record InsertResult ⦃ _ : Ord A ⦄ (x : A) (v : SortedVec A (ℕ.suc n)) : Set where
    constructor mkInsertResult
    field
      vec : SortedVec A (ℕ.suc (ℕ.suc n))
      -- | Stores the information if the inserted element is the new head
      -- or if the head is still the same.
      --
      -- We need this because we need to access the head after we call insert recursively.
      -- This is the reason we have to index SortedVec by it's head.
      newHeadIsNewOrOld : head vec ≡ x ⊎ head vec ≡ head v

  -- We need a helper since rewrite did not work in @insert@.
  ≤-on-≡ : {A : Set} ⦃ _ : Ord A ⦄ → {x y z : A} → x ≤ y → z ≡ x → z ≤ y
  ≤-on-≡ x≤y x≡z rewrite x≡z = x≤y

  insert′ : ⦃ _ : Ord A ⦄
          → (x : A)
          → (v : SortedVec A (ℕ.suc n))
          → InsertResult x v
  insert′ x xs@((y ∷ ys) {newHeadIsBigger = head-ys≤y}) with y ≤! x
  ... | inj₁ y≤x = mkInsertResult ((x ∷ xs) {newHeadIsBigger = y≤x}) (inj₁ refl)
  ... | inj₂ x≤y with insert′ x ys
  ... | mkInsertResult v@(h₁ ∷ vs) (inj₁ head≡x) -- rewrite head≡x does not work, therefore the helper
    = mkInsertResult ((y ∷ v) {newHeadIsBigger = ≤-on-≡ x≤y head≡x}) (inj₂ refl)
  ... | mkInsertResult v@(h₁ ∷ vs) (inj₂ head≡head) -- rewrite head≡head does not work, therefore the helper
    = mkInsertResult ((y ∷ v) {newHeadIsBigger = ≤-on-≡ head-ys≤y head≡head}) (inj₂ refl)
  insert′ x (y ∷[]) with x ≤! y
  ... | inj₁ x≤y = mkInsertResult ((y ∷ (x ∷[])) {x≤y}) (inj₂ refl)
  ... | inj₂ y≤x = mkInsertResult ((x ∷ (y ∷[])) {y≤x}) (inj₁ refl)

insert : ⦃ _ : Ord A ⦄
       → A
       → SortedVec A n
       → SortedVec A (ℕ.suc n)
insert x [] = x ∷[]
insert x v@(_ ∷[]) with insert′ x v
...                   | mkInsertResult vec₁ _ = vec₁
insert x v@(_ ∷ _) with insert′ x v
...                   | mkInsertResult vec₁ _ = vec₁

sort : ⦃ _ : Ord A ⦄
     → (v : Vec A (ℕ.suc n))
     → SortedVec A (ℕ.suc n)
sort (x ∷ []) = x ∷[]
sort (x ∷ xs@(_ ∷ _)) with sort xs
...                      | v = insert x v

isSorted? : ⦃ _ : Ord A ⦄
          → Vec A n
          → Maybe (SortedVec A n)
isSorted? [] = just []
isSorted? (x ∷ []) = just (x ∷[])
isSorted? (x ∷ xs@(_ ∷ _)) with isSorted? xs
...                           | just (y ∷[]) with y ≤? x
...                                             | no _ = nothing
...                                             | yes y≤x = just ( ((x ∷ y ∷[]) {newHeadIsBigger = y≤x}))
isSorted? (x ∷ _ ∷ _) | just ((xs@(y ∷ ys))) with y ≤? x
...                                             | no _    = nothing
...                                             | yes y≤x = just (((x ∷ xs) {newHeadIsBigger = y≤x}))
isSorted? (x ∷ _ ∷ _) | nothing = nothing

open import Data.List using (List; []; _∷_)
import Data.List as List
open import Data.Unit using (⊤)
open import Data.Product using (_×_; _,_)

-- | @SortedVec@ but with the length and head index as runtime witness.
record SomeSortedVec (A : Set) ⦃ _ : Ord A ⦄ : Set where
  constructor mkSome
  field
    {length} : ℕ
    vec      : SortedVec A length

-- | @mkSome@ with explicit @length@.
mkSome′ : ⦃ _ : Ord A ⦄ → (n : ℕ) → SortedVec A n → SomeSortedVec A
mkSome′ _ = mkSome

fromList : ⦃ _ : Ord A ⦄ → (l : List A) → Maybe (SomeSortedVec A)
fromList = Data.Maybe.map mkSome ∘ isSorted? ∘ Vec.fromList
