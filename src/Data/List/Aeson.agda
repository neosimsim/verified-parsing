module Data.List.Aeson where

open import Function using (_$_; _∘_)
open import Data.List using (List; map)

import Foreign.Haskell.Data.Vector as Vector
import Foreign.Haskell.Data.Aeson as Aeson
open Aeson using (_<$>_; mapM; withArray; FromJSON; parseJSON; ToJSON; toJSON)

instance
  ListToJSON : {A : Set} ⦃ _ : ToJSON A ⦄ → ToJSON (List A)
  toJSON ⦃ ListToJSON ⦄ = Aeson.array ∘ Vector.fromList ∘ map toJSON

  ListFromJSON : {A : Set} ⦃ _ : FromJSON A ⦄ → FromJSON (List A)
  parseJSON ⦃ ListFromJSON ⦄ = withArray "List" $ λ xs → Vector.toList <$> mapM parseJSON xs
